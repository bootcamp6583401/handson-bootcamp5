import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Main
 */
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number;
        // HANNDSON 1, 2
        try {
            System.out.print("Masukkan angka: ");
            number = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            number = 0;
            System.out.println("Invalid. Not a number.");
        }

        try {
            if (!NotPrimeException.isPrime(number)) {
                throw new NotPrimeException();
            }
        } catch (NotPrimeException e) {
            System.out.println(e.getMessage());
        }

        // HANDSON 3, 4
        FileReader reader = null;
        try {
            reader = new FileReader("4.txt");
            int data = reader.read();
            while (data != -1) {
                System.out.print((char) data);
                data = reader.read();
            }

        } catch (FileNotFoundException e) {
            System.out.println("FIle not found");
        } catch (IOException e) {
            System.out.println("Error while reading file");
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                System.out.println("Something went wrong while trying to close file reader.");
            }
        }

        // HANDSON 5

        try {
            System.out.print("Masukkan angka lagi dong: ");
            int newNumber = Integer.parseInt(scanner.nextLine());
            if (!NotPrimeException.isPrime(newNumber)) {
                throw new NotPrimeException();
            }
        } catch (Exception e) {
            if (e instanceof NumberFormatException) {
                System.out.println("Bukan angka!");
            } else if (e instanceof NotPrimeException) {
                System.out.println(e.getMessage());
            } else {
                System.out.println("Some other Exception terjadi: " + e.getMessage());
            }
        }

        scanner.close();

    }
}